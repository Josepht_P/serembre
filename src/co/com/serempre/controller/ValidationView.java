package co.com.serempre.controller;



import java.io.Serializable;
import java.util.List;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.xml.soap.SOAPException;

import co.com.serempre.dto.ListTask;
import co.com.serempre.dto.Task;



@ManagedBean
@RequestScoped
public class ValidationView  {
     
	/**
	 * 
	 */
	
	private Task task ;
	private ListTask listTask = ListTask.getListTask();

	
	
    public ValidationView() {		
    	task = new Task();
	}
	
	public Task getTask() {
		return task;
	}
	
	public void setTask(Task task) {
		this.task = task;
	} 
	
	
	public void addTask() {
		System.out.println(task.getTitle());
		listTask.addTask(task);
		
	}
	
	

	public List<Task> getListTask() {
		return listTask.getTasks();
	}

	public void updateTask() {
		listTask.updateTask(task);		
	}
	

    
}
