package co.com.serempre.webService;

import javax.jws.WebMethod;
import javax.jws.WebService;

import co.com.serempre.dto.ListTask;
import co.com.serempre.dto.Task;



@WebService()
public class webServiceTask {
	
	private Task task = new Task();
	
	private ListTask listTask= ListTask.getListTask();
	
	
	@WebMethod(operationName = "addTask")
	public String addTask(String title, int estimatedtime,	 String description) {
		String result= "Ok";
		if (title.length()>16) {
			return result ="El titulo debe ser menor a 16 carácteres";
		}		
		task.setDescription(description);
		task.setTitle(title);
		task.setEstimatedtime(estimatedtime);
		listTask.addTask(task);
		return result;
	}
	
	

}
