package co.com.serempre.dto;

import java.io.Serializable;

public class Task  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String title;
	private Integer estimatedtime;
	private float workedTime;
	private String description;
	private float remainingTime;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getEstimatedtime() {
		return estimatedtime;
	}
	public void setEstimatedtime(Integer estimatedtime) {
		this.estimatedtime = estimatedtime;
	}
	public float getWorkedTime() {
		return workedTime;
	}
	public void setWorkedTime(float workedTime) {
		this.workedTime = workedTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public float getRemainingTime() {
		return remainingTime;
	}
	public void setRemainingTime(float remainingTime) {
		this.remainingTime = remainingTime;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getTruncateDescription() {
		
		return this.description.length()>16 ?  this.description.substring(0,16).concat("..."): this.description;		
	}
	
	
	
	

}
