package co.com.serempre.dto;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.tempuri.CalculatorSoapProxy;



public class ListTask {

	private static ListTask listTask;
	private List<Task> tasks;

	private ListTask() {
		tasks = new ArrayList<Task>();
	}

	public static ListTask getListTask() {
		if (listTask == null) {
			listTask = new ListTask();
		}
		return listTask;

	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void addTask(Task task) {
		task.setId(this.tasks.isEmpty() ? 0 : this.tasks.size());
		this.tasks.add(task);
	}

	public void updateTask(Task task) {
		task.setRemainingTime(calculateRemainingTime(task));
		tasks.set(task.getId(), task);
		System.out.println(task.getId());
	}

	private float calculateRemainingTime(Task task) {
		float result = 0;
		try {
			CalculatorSoapProxy calculatorSoapProxy = new CalculatorSoapProxy();

			String str = String.valueOf(task.getWorkedTime());

			int intNumber = Integer.parseInt(str.substring(0, str.indexOf('.')));

			int decNumberInt = Integer.parseInt(str.substring(str.indexOf('.') + 1));

			int resultAux = calculatorSoapProxy.subtract(task.getEstimatedtime(), intNumber + 1);

			int resultDec = 0;

			switch (str.substring(str.indexOf('.') + 1).length()) {
			case 1:
				resultDec = calculatorSoapProxy.subtract(EnumNumber._DECENA.getNumber(), decNumberInt);
				break;
			case 2:
				resultDec = calculatorSoapProxy.subtract(EnumNumber._CENTENAS.getNumber(), decNumberInt);
				break;
			case 3:
				resultDec = calculatorSoapProxy.subtract(EnumNumber._MIL.getNumber(), decNumberInt);
				break;

			case 4:
				resultDec = calculatorSoapProxy.subtract(EnumNumber._DECENA_MIL.getNumber(), decNumberInt);
				break;
			case 5:
				resultDec = calculatorSoapProxy.subtract(EnumNumber._CENTENA_MIL.getNumber(), decNumberInt);
				break;
			case 6:
				resultDec = calculatorSoapProxy.subtract(EnumNumber._MILLON.getNumber(), decNumberInt);
				break;
			default:
				resultDec = calculatorSoapProxy.subtract(EnumNumber._MILLON.getNumber(), decNumberInt);
				break;

			}

			result = Float.parseFloat(resultAux + "." + resultDec);

		} catch (RemoteException e) {
			e.printStackTrace();
		}

		return result;

	}

}
