package co.com.serempre.dto;

public enum EnumNumber {
	
	_DECENA(10),
	_CENTENAS(100),
	_MIL(1000), 
	_DECENA_MIL(10000),
	_CENTENA_MIL(100000),
	_MILLON(1000000);

	private int number;
	
	EnumNumber( int number) {
		this.number = number;
		
	}

	public int getNumber() {
		return number;
	}

	
	
	
	
	
	
}
